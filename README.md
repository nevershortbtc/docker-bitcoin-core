This document is a very rough draft.

Create docker images for bitcoin-core.

Two docker images are created, one running bitcoind, and one that can be used to run bitcoin-cli.

# Building
You will need docker installed to build the docker images.
The Makefile will create a docker image called 'bitcoin-core-build' and then run this image in a container.
This container will download bitcoin-core and compile it. From the container the Makefile will copy 'bitcoind' and
'bitcoin-cli'. These two files will be placed in two new, separate containers.

The Makefile will then create a docker image called 'bitcoind'. This image can be used to run a bitcoin full node.
The Makefile will also create a docker image called 'bitcoin-cli'. This image is intended to be run and removed only when
needed.

# Running bitcoind
docker run -it --rm -v bitcoin:/bitcoin -p 8332:8332 -p 8333:8333 bitcoind

# Running bitcoin-cli
docker run -it --rm -v bitcoin:/bitcoin --network host bitcoin-cli help
