all:
				$(MAKE) -C bitcoin-build
				cp bitcoin-build/bitcoind bitcoind-image/
				cp bitcoin-build/bitcoin-cli bitcoin-cli-image/
				$(MAKE) -C bitcoind-image
				$(MAKE) -C bitcoin-cli-image

clean:
				$(MAKE) clean -C bitcoin-build
				$(MAKE) clean -C bitcoind-image
				$(MAKE) clean -C bitcoin-cli-image

dist-clean:
				$(MAKE) distclean -C bitcoin-build
				$(MAKE) distclean -C bitcoind-image
				$(MAKE) distclean -C bitcoin-cli-image
