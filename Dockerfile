# bitcoin-core in docker
# compile from source and create a slim image for bitcoin-core
# long@nevershortbtc.com

# global, should not need changing
ARG USERNAME="satoshi"
ARG MOUNTPOINT="/bitcoin"

FROM alpine:3.16 AS builder

# output of latest.sh
ARG LATEST="bitcoin-core-23.0"

# the directory it will extract to
# should be ${LATEST} with "-core" removed
ARG DIRNAME="bitcoin-23.0"

# these should not need changing
ARG FILENAME="${DIRNAME}.tar.gz"
ARG URL="https://bitcoincore.org/bin/${LATEST}/${FILENAME}"
ARG USERNAME
ARG MOUNTPOINT

RUN apk --no-cache update && apk --no-cache add curl autoconf automake pkgconf libtool gawk make g++ db-dev libevent-dev boost-dev

# Add satoshi user and its bin directory
RUN adduser -s /bin/sh -D ${USERNAME}
RUN mkdir /home/${USERNAME}/bin
RUN chown ${USERNAME}:${USERNAME} /home/${USERNAME}/bin

# Build bitcoin-core
USER ${USERNAME}
WORKDIR /home/${USERNAME}
RUN curl -O ${URL}
RUN tar zxf ${FILENAME}
WORKDIR /home/${USERNAME}/${DIRNAME}
RUN ./autogen.sh
RUN ./configure
RUN make
RUN cp src/bitcoind /home/${USERNAME}/bin/
RUN cp src/bitcoin-cli /home/${USERNAME}/bin/
RUN strip /home/${USERNAME}/bin/bitcoind
RUN strip /home/${USERNAME}/bin/bitcoin-cli

################
# Second stage #
################
FROM alpine:3.16

ARG USERNAME
ARG MOUNTPOINT

VOLUME ${MOUNTPOINT}
EXPOSE 8332
EXPOSE 8333

RUN apk --no-cache update && apk --no-cache add libevent libstdc++

# Add satoshi user and its bin directory
RUN adduser -s /bin/sh -D ${USERNAME}
RUN mkdir /home/${USERNAME}/bin
RUN chown ${USERNAME}:${USERNAME} /home/${USERNAME}/bin

# make the location of bitcoind's data convenient for docker use
#RUN mkdir ${MOUNTPOINT}
RUN chown ${USERNAME}:${USERNAME} ${MOUNTPOINT}
RUN ln -s ${MOUNTPOINT} /home/${USERNAME}/.bitcoin

COPY --from=builder /home/${USERNAME}/bin/bitcoind /home/${USERNAME}/bin/
COPY --from=builder /home/${USERNAME}/bin/bitcoin-cli /home/${USERNAME}/bin/

USER ${USERNAME}
ENV PATH="${PATH}:/home/${USERNAME}/bin"
CMD "/home/${USERNAME}/bin/bitcoind"

